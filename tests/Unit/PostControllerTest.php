<?php

namespace Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

use App\Role;
use App\User;
use App\Post;

class PostControllerTest extends TestCase
{
    use RefreshDatabase;

    protected $user;

    protected function setUp(): void
    {
        parent::setUp();

        factory(Role::class)->create();
        $this->user = factory(User::class)->create();
    }

    /**
     * Post creation unit test.
     *
     * @return void
     */
    public function testCreate()
    {
        $post_data = [
            'title' => 'Post Title',
            'text' => 'Post Text',
            'user_id' => $this->user->id
        ];

        $response = $this->actingAs($this->user)->post('/admin/create-post', $post_data);

        $response->assertStatus(302); // Redirect back
        $this->assertDatabaseHas('posts', $post_data);
    }

    public function testUpdate()
    {
        $post = factory(Post::class)->create();

        $new_text = 'Lorem ipsum dolor sit amet.';

        $response = $this->actingAs($this->user)->post('/admin/edit-post', [
            'post_id' => $post->id,
            'title' => $post->title,
            'text' => $new_text
        ]);

        $response->assertStatus(302); // Redirect back
        $this->assertDatabaseHas('posts', [
            'id' => $post->id,
            'text' => $new_text
        ]);
    }
}
