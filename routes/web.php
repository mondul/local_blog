<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

Auth::routes();

Route::group([
	'prefix' => '/admin',
	'middleware' => 'auth'
], function() {
	Route::get('/', 'AdminController@index')->name('admin');

	Route::view('/create-post', 'create_edit')->name('create_view');
	Route::get('/edit-post/{post_id}', 'PostController@editView')->where('post_id', '[0-9]+')->name('edit_view');
	Route::post('/create-post', 'PostController@create')->name('create_post');
	Route::post('/edit-post', 'PostController@edit')->name('edit_post');
});
