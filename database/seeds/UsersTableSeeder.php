<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $now = Carbon\Carbon::now()->toDateTimeString();

        DB::table('users')->insert([
            'id' => 1,
            'name' => 'Super Admin',
            'email' => 'super_admin@local.blog',
            'email_verified_at' => $now,
            'password' => bcrypt('123456'),
            'role_id' => 1,
            'created_at' => $now,
            'updated_at' => $now
        ]);

        DB::table('users')->insert([
            'id' => 2,
            'name' => 'User Test',
            'email' => 'user.test.12@mail.com',
            'email_verified_at' => $now,
            'password' => bcrypt('123456'),
            'created_at' => $now,
            'updated_at' => $now
        ]);
    }
}
