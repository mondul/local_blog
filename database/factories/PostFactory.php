<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Post;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Post::class, function (Faker $faker) {
	$text = '';

	for ($i = 0; $i < 3; $i++) {
		$text .= '<p>' . $faker->sentence(20) . '</p>';
	}

    return [
        'title' => $faker->sentence(),
        'text' => $text,
        'user_id' => 2,
    ];
});
