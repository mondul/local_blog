<?php

namespace App\Http\Controllers;

use Auth;

class AdminController extends Controller
{
    /**
     * Show the current user's posts.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Auth::user()->posts->sortByDesc('id');
        return view('admin', compact('posts'));
    }
}
