<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Post;

class PostController extends Controller
{
    /**
     * Show the edit post view.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function editView($post_id)
    {
        $user = Auth::user();

        $opts = [ 'id' => $post_id ];

        if ($user->role_id > 1) {
            $opts['user_id'] = $user->id;
        }

        $post = Post::where($opts)->first();
        return view('create_edit', compact('post', 'post_id'));
    }

    public function create(Request $request)
    {
        $post = new Post();

        $post->title = $request->title;
        $post->text = $request->text;
        $post->user_id = Auth::user()->id;
        $post->save();

        return redirect()->route('admin');
    }

    public function edit(Request $request)
    {
        $post = Post::find($request->post_id);

        $post->title = $request->title;
        $post->text = $request->text;
        $post->save();

        return redirect()->route('admin');
    }
}
