@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">My Posts</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table table-hover">
                        <thead>
                            <tr>
                                <th>Id</th>
                                <th>Title</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th></th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($posts as $post)
                            <tr>
                                <td>{{ $post->id }}</td>
                                <td>{{ $post->title }}</td>
                                <td>{{ $post->created_at }}</td>
                                <td>{{ $post->updated_at }}</td>
                                <td><a href="/admin/edit-post/{{ $post->id }}">Edit</a></td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <p>&nbsp;</p>

                    <div class="text-right">
                        <a href="{{ route('create_view') }}" class="btn btn-primary">Create New Post</a>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
