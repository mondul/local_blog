@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ isset($post) ? 'Edit' : 'Create' }} Post</div>

                <form class="card-body" method="POST" action="{{ route(isset($post) ? 'edit_post' : 'create_post') }}">
                    @csrf

                    @if (isset($post))
                    <input type="hidden" name="post_id" value="{{ $post_id }}">
                    @endif

                    <div class="form-group">
                        <label for="title">Post Title</label>
                        <input type="text" id="title" class="form-control" name="title" placeholder="Enter the post's title"{!! isset($post) ? ' value="' . htmlspecialchars($post->title) . '"' : '' !!} required>
                    </div>

                    <div class="form-group">
                        <label for="text">Post Text</label>
                        <textarea class="form-control" id="text" name="text" rows="14">{!! isset($post) ? $post->text : '' !!}</textarea>
                    </div>

                    <div class="text-right">
                        <button type="submit" id="submit" class="btn btn-primary">Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="https://unpkg.com/tinymce@5.0.16/tinymce.min.js"></script>
<script>
tinymce.init({ selector: '#text' });
</script>
@endsection
