@extends('layouts.app')

@section('content')
<div class="container">
    @foreach($posts as $post)
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ $post->title }}</div>

                <div class="card-body">
                    {!! $post->text !!}
                    <p class="created">Created by {{ $post->user->name }} at {{ Carbon\Carbon::parse($post->updated_at)->isoFormat('MMMM D / YYYY, h:mm A') }}</p>
                </div>
            </div>
        </div>
    </div>
    <p>&nbsp;</p>
    @endforeach
</div>
@endsection
